# ReactNative -Tulisin
Repository ini digunakan untuk dokumentasi final project Bootcamp Sanbercode Kelas Intensif React Native Batch 38. Aplikasi ini terhubung dengan database yang disediakan melalui Firebase (menggunaakn Firebase Rest API).

### Mock Up on Figma
https://www.figma.com/file/aOeCG1ukn3fQO3iQW6pNgB/tulis.in?node-id=1%3A8https://www.figma.com/file/aOeCG1ukn3fQO3iQW6pNgB/tulis.in?node-id=1%3A8

### Demo on Youtube
https://youtu.be/RqSI7QoESNI

### Download APK/IPA via Google Drive
https://drive.google.com/drive/folders/1uCuAdtvhnWyGmcT2RYmLf0ivjjsfSo_n?usp=sharing