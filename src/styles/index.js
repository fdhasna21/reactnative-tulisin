import { StyleSheet, Dimensions } from "react-native"

const color = {
    primary :  "#506DBE",
    secondary : "#172753",
    accent : "#EFF3F5",
    gray : "#A8AAAB"
}

const styles = StyleSheet.create({
    background : {
        container : {
            width : Dimensions.get('screen').width,
            height : Dimensions.get('screen').height,
            flex : 1    
        },
        content : {
            padding : 24
        },
        bg2 : {
            title : {
                marginTop : 120,
                marginBottom : 24,
                fontFamily: "Roboto",
                fontStyle: "normal",
                fontWeight: "700",
                fontSize: 32,
                lineHeight: 38,
                color: color.primary,
                borderStyle: "solid",
                textShadowOffset: {
                width: 0,
                height: 3
                }
            }
        }
    },
    image :{
        logo : {
            marginTop : 120,
            alignSelf : "center"
        },
        landing : {
            width : Dimensions.get('screen').width,
        },
        backButton : {
            padding : 12
        },
        about : {
            logo : {
                alignSelf : "center",
                height : 100,
                marginTop : -10,
                resizeMode : "contain"
            },
            skill : {
                marginHorizontal : -12,
                width : 200,
                height : 100,
                marginVertical : 4
            },
            contact : {
                width : 80, 
                height : 80
            }
        },
        post : {
            alignSelf : "center",
            width : Dimensions.get('screen').width,
            height : 200,
            marginBottom : 12
        }
    },
    button : {
        container : {
            padding : 16,
            marginVertical : 12,
            borderWidth : 1,
            borderRadius : 4,
            borderColor : color.primary,
            backgroundColor : color.primary
        },
        containerOutline : {
            padding : 16,
            margin : 12,
            borderWidth : 1,
            borderRadius : 4,
            borderColor : "white"
        },
        text : {
            color : "white",
            textAlign : "center",
            fontFamily : "Roboto",
            fontStyle : "normal",
            fontWeight : "bold",
            fontSize : 14,
            lineHeight : 22,
            textAlign : "center",
            letterSpacing : 0.1,
            textTransform: 'uppercase'
        },
        textAsButton : {
            padding : 4,
            fontFamily: "Roboto",
            fontStyle: "normal",
            fontWeight: "bold",
            fontSize: 12,
            lineHeight: 16,
            letterSpacing: 0.8,
            color: color.primary,
            alignSelf: 'flex-end'
        }
    },
    textInput : {
        container : {
            marginVertical : 4
        },
        border : {
            height: 48,
            borderWidth: 1,
            padding: 10,
            borderRadius : 5,
            borderColor : "gray"
        },
        label : {
            color : "gray",
            alignSelf: 'flex-start',
            fontSize: 12,
            lineHeight: 16,
            padding : 4
        },
        post : {
            title : {
                fontSize : 20,
                marginBottom : 12
            },
            content : {
                fontSize : 14,
                textAlignVertical: "top",
                marginBottom : 12
            }
        }
    },
    text : {
        paragraph : {
            fontStyle : "normal",
            fontSize : 12,
            lineHeight : 22,
            letterSpacing : 0.1,
            paddingVertical : 4,
            flex : 1,
            flexWrap : "wrap"
        },
        subtitle : {
            fontStyle : "normal",
            fontSize : 14,
            lineHeight : 22,
            flex : 1,
            flexWrap : "wrap",
            fontWeight : "bold",
            color : color.primary,
        },
        caption : {
            fontStyle : "normal",
            fontSize : 12,
            lineHeight : 22,
            flex : 1,
            flexWrap : "wrap",
            textTransform : "uppercase",
            color : "gray"
        }
    },
    fab : {
        container : {
            justifyContent : "center",
            alignItems : "center"
        },
        content : {
            width : 70,
            height : 70,
            borderRadius : 35,
            backgroundColor : color.primary
        }
    },
    divider : {
        marginVertical : 8,
        height : 0.4,
        backgroundColor : color.secondary
    },
    card : {
        outside : {
            margin : 4,
            elevation : 1.5,
            flexWrap : "wrap",
            flexDirection : "row"
        },
        inside : {
            padding : 12,
            width : 200,
            borderWidth : 0.1,
            borderRadius : 1,
            backgroundColor : color.accent
        }
    },
})

const img = {
    logo : require("../images/logos/logo1.png"),
    landing : require("../images/others/img-landing.png"),
    bg1 : require("../images/backgrounds/bg1.png"),
    bg2 : require("../images/backgrounds/bg2.png"),
    bg3 : require("../images/backgrounds/bg3.png"),
    andstu : require("../images/others/android-studio.png"),
    reactjs : require("../images/others/react-js.png"),
    reactnative : require("../images/others/react-native.png"),
    authorProfile : require("../images/others/author-profile.png"),
    iconGmail : require("../images/icons/Gmail.png"),
    iconLinkedin : require("../images/icons/Linkedin.png"),
    iconWhatsapp : require("../images/icons/Whatsapp.png"),
    iconTelegram : require("../images/icons/Telegram.png"),
    iconGithub : require("../images/icons/Github.png"),
    iconGitlab : require("../images/icons/Gitlab.png")
}

module.exports = { color, styles, img }