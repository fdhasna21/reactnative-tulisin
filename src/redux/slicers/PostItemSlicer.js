import { createSlice } from "@reduxjs/toolkit"
import { ToastAndroid } from "react-native"

export const postItemSlice = createSlice({
    name : "postItem",
    initialState : {
        postId : "",
        title : "",
        content : "",
        author : "",
        created : "",
        image : "",
        authorId : "",
        isEditable : true,
        isValid : false,
        isPostSubmited : false
    },
    reducers : {
        setPostSubmited : (state, action) => {
            state.isValid = false
            resetItem()
        },
        setItem : (state, action) => {
            const data = action.payload.data
            const auth = action.payload.auth
            if(data != null || data != undefined){
                state.postId = data.postId
                state.title = data.title
                state.content = data.content
                state.author = data.author
                state.created = data.created
                state.authorId = data.authorId
                state.image = data.image
                if(auth.id == data.authorId){
                    state.isEditable = true
                } else {
                    state.isEditable = false
                }
            }
        },
        setTitle : (state, action) => {
            state.title = action.payload
        },
        setContent : (state, action) => {
            state.content = action.payload
        },
        submitPost : (state) => {
            if(state.title == "" || state.content == ""){
                ToastAndroid.show("Field can not be null", ToastAndroid.SHORT)
                state.isValid = false
            } else {
                state.isValid = true
            }
        },
        resetItem : (state, action) => {
            state.postId = ""
            state.title = ""
            state.content = ""
            state.author = action.payload.id
            state.created = ""
            state.authorId = ""
            state.image = ""
            state.isEditable = true
            state.isPostSubmited = false
            state.isValid = false
        }
    }
})

export const { setItem, setTitle, setContent, setPostSubmited, resetItem, submitPost } = postItemSlice.actions

export default postItemSlice.reducer