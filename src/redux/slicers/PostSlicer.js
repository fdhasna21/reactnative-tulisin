import { createSlice } from "@reduxjs/toolkit"

export const postSlice = createSlice({
    name : "post",
    initialState : {
        allPosts : null,
        myPosts : null,
        isPostFetched : true
    },
    reducers : {
        setPosts : (state, action) => {
            console.log(action.payload)
            const array = []
            if(action.payload != null){
                const obj = action.payload
                Object.keys(obj).forEach((key) => {
                    const subObject = obj[key]
                    subObject["postId"] = key
                    array.push(subObject)
                })
                
            }
            state.allPosts = array
            console.log("allPosts", state.allPosts)
            state.isPostFetched = false 
        },
        getPosts : (state) => {
            state.isPostFetched = true
        },
        getMyPosts : (state, action) => {
            let array = []
            if(action.payload != null){
                const auth = action.payload
                array = state.allPosts.filter(obj => {
                        return obj.authorId == auth.id
                    })
            }
            state.myPosts = array
            console.log("myPosts", state.myPosts)        
        }
    }
})

export const { setPosts, getPosts, getMyPosts } = postSlice.actions

export default postSlice.reducer