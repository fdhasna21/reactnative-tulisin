import { createSlice } from '@reduxjs/toolkit'
import { ToastAndroid } from 'react-native'

export const authSlice = createSlice({
    name : "auth",
    initialState : {
        id : "",
        email : "",
        password : "",
        passwordConfirmation : "",
        isUserSigned : false,
        isValid : false
    },
    reducers : {
        setInputNotValid : (state, action) => {
            state.isValid = false
            if(action.payload.signed == true) {
                state.isUserSigned = true
                state.id = action.payload.id
                state.password = ""
                state.passwordConfirmation = ""
                state.isValid = false
            }
        },
        setEmail : (state, action) => {
            state.email = action.payload
        },
        setPassword : (state, action) => {
            state.password = action.payload
        },
        setPasswordConfirmation : (state, action) => {
            state.passwordConfirmation = action.payload
        },
        submitSignIn : (state) => {
            if(state.email == "" || state.password == ""){
                ToastAndroid.show("Field cannot be null", ToastAndroid.SHORT)
                state.isValid = false
            } else {
                state.isValid = true
            }
        },
        submitSignUp : (state) => {
            if(state.email == "" || state.password == "" || state.passwordConfirmation == ""){
                ToastAndroid.show("Field cannot be null", ToastAndroid.SHORT)
                state.isValid = false
            } else if (state.password != state.passwordConfirmation){
                ToastAndroid.show("Passwords are not match", ToastAndroid.SHORT)
                state.isValid = false
            } else {
                state.isValid = true
            }
        },
        submitSignOut : (state) => {
            state.id = ""
            state.email = ""
            state.isUserSigned = false
        },
        resetAuth : (state) => {
            state.id = ""
            state.email = ""
            state.password = ""
            state.passwordConfirmation = ""
            state.isUserSigned = false
            state.isUserSigned = false
        }
    }
})

export const { setEmail, setPassword, setPasswordConfirmation, setInputNotValid, submitSignIn, submitSignUp, submitSignOut, resetAuth } = authSlice.actions

export default authSlice.reducer