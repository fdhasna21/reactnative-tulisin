import { configureStore } from '@reduxjs/toolkit'
import { authSlice } from './slicers/AuthSlicer'
import { postItemSlice } from './slicers/PostItemSlicer'
import { postSlice } from './slicers/PostSlicer'

export const store = configureStore({ 
    reducer : {
        [authSlice.name] : authSlice.reducer,
        [postSlice.name] : postSlice.reducer,
        [postItemSlice.name] : postItemSlice.reducer
    }})