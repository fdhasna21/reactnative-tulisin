import { View, Text, TouchableHighlight, Image } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../styles')
const PostItemComponent = ( props ) => {
    return (
        <TouchableHighlight 
            activeOpacity={0.6}
            underlayColor={color.gray}
            onPress={() => props.onPress()}
            style={{ paddingVertical : 10, paddingHorizontal : 24 }}>
                <>
                    <View style={{flexDirection : "row"}}>
                        {
                            props.item.image != ""
                            ? <View style={{marginRight : 12}}>
                                <Image 
                                    source={{ uri : props.item.image }}
                                    style={{ 
                                        width : 90,
                                        height:90
                                    }}
                                    resize="contain"/>
                            </View>
                            : null
                        }
                        <View style={{ flexShrink: 1 }}>
                            <Text 
                                style={styles.text.subtitle}
                                numberOfLines={1}>{props.item.title}</Text>
                            <Text 
                                style={styles.text.paragraph}
                                numberOfLines={3}>{props.item.content}</Text>
                        </View>
                    </View>
                    <Text style={{...styles.text.caption, fontWeight : "bold", textTransform : "none"}}>{props.item.author} • {props.item.created}</Text>
                </>
      </TouchableHighlight> 
    )
}

export default PostItemComponent