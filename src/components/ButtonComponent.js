import { TouchableHighlight, Text } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../styles')
const ButtonComponent = (props) => {

    let style
    if(props.outlined != true){
        style = {...styles.button.container, ...props.style}
    } else {
        style = {...styles.button.containerOutline, ...props.style}
    }

    return (
        <TouchableHighlight
            style={style}
            onPress={() => props.onPress()}
            underlayColor={color.secondary}>
            <Text style={styles.button.text}>{props.title}</Text>
        </TouchableHighlight>
    )
}

export default ButtonComponent