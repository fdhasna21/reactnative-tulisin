import { View, TouchableOpacity, Image, Text } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../styles')
const ButtonCircleComponent = (props) => {
    let opacity = props.activeOpacity != undefined ? props.activeOpacity : 0.5
    return (
        <TouchableOpacity
            style={{...styles.fab.container, ...props.style}}
            onPress={() => props.onPress()}
            activeOpacity={opacity}>
            {
                props.layout != undefined 
                ? <View style={{...styles.fab.content}}>
                      {props.layout}
                  </View>
                : <Image 
                    style={props.contentStyle}
                    source={props.image}
                    resizeMode="stretch"/>
            }
        </TouchableOpacity>
    )
}

export default ButtonCircleComponent