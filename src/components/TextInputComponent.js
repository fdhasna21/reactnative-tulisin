import { View, Text, TextInput } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../styles')
const TextInputComponent = (props) => {
    return (
        <View style={styles.textInput.container}>
            <Text style={styles.textInput.label}>{props.title}</Text>
            <TextInput
                style={styles.textInput.border}
                value={props.value}
                onChangeText={(value) => props.changeText(value)}/>
        </View>
    )
}

export default TextInputComponent