import { ImageBackground, StatusBar, ScrollView, Image, View } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../../styles')
const BaseBgV1 = ( props ) => {
    return (
        <ImageBackground
            source={img.bg1}
            resizeMode="stretch"
            style={styles.background.container}>
                <StatusBar translucent backgroundColor="transparent" />
                <ScrollView >            
                    <View style={styles.background.content}>
                        { props.layout }
                    </View>
                    <Image
                        source={img.landing}
                        style={styles.image.landing}/>
                </ScrollView>
                
        </ImageBackground>
    )
}

export default BaseBgV1