import { ImageBackground, StatusBar, ScrollView, View } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../../styles')
const BaseBgV3 = ( props ) => {
    return (
        <ImageBackground
            source={img.bg3}
            resizeMode="stretch"
            style={styles.background.container}>
                <StatusBar translucent backgroundColor="transparent" />
                {
                    props.scrollView != false 
                    ? <ScrollView style={styles.background.content}>
                            { props.layout }
                      </ScrollView>
                    : <View style={styles.background.content}>
                            { props.layout }
                      </View>
                }
        </ImageBackground>
    )
}

export default BaseBgV3