import { Text, ImageBackground, StatusBar, ScrollView } from 'react-native'
import React from 'react'

const {styles, color, img} = require('../../styles')
const BaseBgV2 = ( props ) => {
    return (
        <ImageBackground
            source={img.bg2}
            resizeMode="stretch"
            style={styles.background.container}>
                <StatusBar translucent backgroundColor="transparent" />
                <ScrollView style={styles.background.content}>
                    <Text style={styles.background.bg2.title}>{props.title}</Text>
                    { props.layout }
                </ScrollView>
        </ImageBackground>
    )
}

export default BaseBgV2