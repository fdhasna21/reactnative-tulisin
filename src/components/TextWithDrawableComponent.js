import { View, Text } from 'react-native'
import React from 'react'
import { Icon } from 'react-native-elements'

const {styles, color, img} = require('../styles')
const TextWithDrawableComponent = (props) => {
    return (
        <View style={{flexDirection : "row"}}>
            <Icon style={{marginRight:8, marginTop:4}} name={props.iconName} type={props.iconType} size={20} color={color.primary}/>
            <Text style={styles.text.paragraph}>{props.text}</Text>
        </View>
    )
}

export default TextWithDrawableComponent