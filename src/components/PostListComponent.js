import { FlatList, View } from 'react-native'
import React from 'react'
import PostItemComponent from './PostItemComponent'

const {styles, color, img} = require('../styles')
const PostListComponent = (props) => {
    return (
        <View style={{ marginHorizontal:-24 }}>
            <FlatList
                data={props.data}
                keyExtractor={item => item.postId}
                renderItem={({item}) =>
                    <PostItemComponent 
                        {...props}
                        item={item}
                        onPress={() => props.navigation.navigate("PostScreen", {
                            data : item
                        })}/>
                }
                ItemSeparatorComponent={() => <View style={styles.divider}></View>}/>
        </View>
    )
}

export default PostListComponent