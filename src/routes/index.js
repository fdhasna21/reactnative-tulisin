import React, { useEffect } from 'react'
import { ToastAndroid, TouchableOpacity, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import { Icon } from 'react-native-elements'
import { Ionicons } from '@expo/vector-icons'
import { HeaderButton, HeaderButtons, Item } from 'react-navigation-header-buttons'
import { useSelector } from 'react-redux'
import LandingScreen from '../layouts/LandingScreen'
import SignUpScreen from '../layouts/SignUpScreen'
import SignInScreen from '../layouts/SignInScreen'
import AboutScreen from '../layouts/AboutScreen'
import HomeScreen from '../layouts/HomeScreen'
import ProfileScreen from '../layouts/ProfileScreen'
import PostScreen from '../layouts/PostScreen'
import ButtonCircleComponent from '../components/ButtonCircleComponent'

const { styles, color, img } = require("../styles")
const Stack = createStackNavigator()
const BottomTab = createBottomTabNavigator()

const index = () => {
    return (
        <NavigationContainer>
            <StackNavigator/>
        </NavigationContainer>
      )
  }

const StackNavigator = () => {
    const auth = useSelector((state) => state.auth)

    return (
        <Stack.Navigator 
            initialRouteName='LandingScreen' 
            screenOptions={{ 
                headerTitle : () => null,
                headerTransparent: true,
                headerShadowVisible: false,
                animationEnabled: false,
                headerStyle: {
                    backgroundColor: color.primary
                },
                headerTintColor: "white",
                headerTitleStyle: {
                    fontWeight: 'bold',
                }}}>
            {
                auth.isUserSigned ? (
                    <Stack.Group>
                        <Stack.Screen 
                            name="MainAppScreen" 
                            component={BottomTabNavigator}
                            options={{
                                headerShown : false
                            }}/>
                        <Stack.Screen name="AboutScreen" component={AboutScreen} options={{
                            headerTransparent : false,
                            headerTitle : "About",
                        }}/>
                        <Stack.Screen name="PostScreen" component={PostScreen} options={({ route }) => ({
                            headerTitle : route.params == undefined ? "Add Post" : "",
                            headerTransparent : false,
                            headerRight : () => (
                                route.params == undefined 
                                ?   <HeaderButtons HeaderButtonComponent={IoniconHeaderButton}>
                                        <Item title="AddPicture" iconName="image-outline" onPress={() => ToastAndroid.show("Coming Soon", ToastAndroid.SHORT)}/>
                                    </HeaderButtons>
                                :   null
                            )
                        })}/>
                    </Stack.Group>
                ) : (
                    <Stack.Group>
                        <Stack.Screen name="LandingScreen" component={LandingScreen}/>
                        <Stack.Screen name="SignUpScreen" component={SignUpScreen}/>
                        <Stack.Screen name="SignInScreen" component={SignInScreen}/>
                    </Stack.Group>
                )
            }
        </Stack.Navigator>
    )
}

const BottomTabNavigator = () => {
    return(
        <BottomTab.Navigator 
            screenOptions={{ 
                tabBarActiveTintColor: "white",
                tabBarInactiveTintColor: "gray",
                tabBarStyle: {
                    backgroundColor: color.secondary,
                },
                headerStyle: {
                    backgroundColor: color.primary
                },
                headerTintColor: "white",
                headerTitleStyle: {
                    fontWeight: 'bold',
                },
            }}>
            <BottomTab.Screen 
                name="HomeScreen" 
                component={HomeScreen}
                options={{ 
                    headerTitle : "Home",
                    tabBarLabel : "Home",
                    tabBarIcon: ({ color, size }) => (
                        <Icon name="home" type="entypo" color={color} size={size} />
                      ), 
                }}/>

            <BottomTab.Screen
                name="AddPostScreen"
                component={PostScreen}
                options={({ navigation }) => ({
                    headerTitle : "Add Post",
                    tabBarLabel : () => null,
                    tabBarIcon : ({ }) => (
                        <Icon name="plus" type="antdesign" color="white" size={30} />
                    ),
                    tabBarButton : (props) => (<BottomTabFAB 
                        {...props}
                        onPress={() => navigation.navigate("PostScreen")}/>)
                })}/>

            <BottomTab.Screen 
                name="ProfileScreen" 
                component={ProfileScreen}
                options={({ route, navigation }) => ({
                    headerTitle : "Profile",
                    tabBarLabel : "Profile",
                    tabBarIcon : ({ color, size }) => (
                        <Icon name="person" type="ionicons" color={color} size={size} />
                    ),
                    headerRight : () => (
                        <HeaderButtons HeaderButtonComponent={IoniconHeaderButton}>
                            <Item title="About" iconName="information-circle-outline" onPress={() => navigation.navigate("AboutScreen")}/>
                        </HeaderButtons>
                    )
                })}/>
        </BottomTab.Navigator>
    )
}

const IoniconHeaderButton = (props) => (
    <HeaderButton IconComponent={Ionicons} iconSize={24} color="white" {...props}/>
)

const BottomTabFAB = ({ children, onPress }) => (
    <ButtonCircleComponent
        style={{ top : -30 }}
        activeOpacity={0.95}
        onPress={onPress}
        layout={children}/>

)

export default index