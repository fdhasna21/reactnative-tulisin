const keyValue = "AIzaSyDK1LvmlSpiJjgkYils-UYNX1mJkxwjIqY"
const key = `?key=${keyValue}`

const endpoint = {
    signin : "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword"+key,
    signup : "https://identitytoolkit.googleapis.com/v1/accounts:signUp" +key,
    posts : "https://react-native-tulisin-default-rtdb.firebaseio.com/posts.json",
    eachpost : "https://react-native-tulisin-default-rtdb.firebaseio.com/posts/"
}

export { endpoint }