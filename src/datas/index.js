import { endpoint } from "./firebase"

const { styles, color, img } = require("../styles")

const authorSkill = [
    {
        id : "android",
        title : "Android Development",
        tools : "with Android Studio",
        image : img.andstu,
        desc : "Kotlin, Java, and XML"
    }, 
    {
        id : "reactjs",
        title : "Website Development",
        tools : "with VSCode + React JS",
        image : img.reactjs,
        desc : "Javascript, CSS, HTML"
    }, 
    {
        id : "reactnative",
        title : "Mobile Apps Development",
        tools : "with VSCode + React Native",
        image : img.reactnative,
        desc : "Javascript"
    }
]

const authorContact = [
    {
        id : "gmail",
        image : img.iconGmail,
        url : ""
    },
    {
        id : "linkedin",
        image : img.iconLinkedin,
        url : "https://www.linkedin.com/in/fernandahasna"
    },
    {
        id : "whatsapp",
        image : img.iconWhatsapp,
        url : "https://api.whatsapp.com/send/?phone=6281212719895&text&type=phone_number&app_absent=0"
    },
    {
        id : "telegram",
        image : img.iconTelegram,
        url : "https://t.me/fernandahasna",
        color : "#37AEE2"
    },
    {
        id : "github",
        image : img.iconGithub,
        url : "https://github.com/fdhasna21",
        color : "#0A0A0A"
    },
    {
        id : "gitlab",
        image : img.iconGitlab,
        url : "https://gitlab.com/fdhasna21",
        color : "#F8A99D"
    }
]

const postDummy = [
    {
        id : "post1",
        title : "Lorem Ipsum",
        content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis iaculis nunc, ut luctus enim. Donec vitae nunc varius, auctor magna nec, scelerisque arcu ....",
        author : "john.doe@gmail.com",
        created : "06 November 2022",
        image : "https://uploads-ssl.webflow.com/60abcbf13c9af64a88390582/6230f5f37b84136a6985b3df_899889.jpg",
        authorId : ""
    },
    {
        id : "post2",
        title : "Lorem Ipsum",
        content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis iaculis nunc, ut luctus enim. Donec vitae nunc varius, auctor magna nec, scelerisque arcu ....",
        author : "john.doe@gmail.com",
        created : "06 November 2022",
        image : null,
        authorId : "author2"
    },
]

export { authorSkill, authorContact, postDummy, endpoint }