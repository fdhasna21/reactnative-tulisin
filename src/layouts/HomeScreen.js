import { View, Text, Image, TouchableHighlight, ToastAndroid } from 'react-native'
import React, { useEffect } from 'react'
import axios from 'axios'
import BaseBgV3 from '../components/base/BaseBgV3'
import { postDummy } from '../datas'
import PostListComponent from '../components/PostListComponent'
import { useDispatch, useSelector } from 'react-redux'
import { setPosts } from '../redux/slicers/PostSlicer'

import { endpoint } from '../datas'
const {styles, color, img} = require('../styles')
const HomeScreen = ({ navigation }) => {
    const post = useSelector((state) => state.post)
    const dispatch = useDispatch()

    useEffect(() => {
        if(post.isPostFetched){
            axios.get(endpoint.posts, {
                headers : { "Content-Type": "application"},
            })
            .then((response) => {
                const data = response.data
                dispatch(setPosts(data))
            })
            .catch((error) => {
                console.log("Request URL", error.response.config.url)
                console.log("Request Body", error.response.config.data)
                console.log("Error Data", error.response.data)
                dispatch(setPosts(null))
            })
        }
    }, [post.isPostFetched])

    return(
        <BaseBgV3
            scrollView={false}
            layout={
                <PostListComponent
                    data={post.allPosts}
                    navigation={navigation}/>
            }
        />
    )
}

export default HomeScreen