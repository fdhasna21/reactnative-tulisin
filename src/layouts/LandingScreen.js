import { Image } from 'react-native'
import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import BaseBgV1 from '../components/base/BaseBgV1'
import ButtonComponent from '../components/ButtonComponent'
import { resetAuth } from '../redux/slicers/AuthSlicer'

const {styles, color, img} = require('../styles')
const LandingScreen = ({ navigation }) => {
    const auth = useSelector((state) => state.auth)
    const dispatch = useDispatch()

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            dispatch(resetAuth())
        })
        return unsubscribe
    }, [navigation])

    return (
        <BaseBgV1
            layout={
                <>
                    <Image
                        source={img.logo}
                        style={styles.image.logo}/>
                    <ButtonComponent
                        title="Sign In"
                        outlined={true}
                        onPress={() => navigation.navigate("SignInScreen")}/>
                    <ButtonComponent
                        title="Sign Up"
                        outlined={true}
                        onPress={() => navigation.navigate("SignUpScreen")}/>
                </>
            }/>
    )
}

export default LandingScreen