import { ToastAndroid, View, Text } from 'react-native'
import React, { useEffect } from 'react'
import axios from 'axios'
import BaseBgV2 from '../components/base/BaseBgV2'
import ButtonComponent from '../components/ButtonComponent'
import TextInputComponent from '../components/TextInputComponent'
import { useSelector, useDispatch } from 'react-redux'
import { setEmail, setPassword, submitSignIn, setInputNotValid } from '../redux/slicers/AuthSlicer'

import { endpoint } from '../datas'
const {styles, color, img} = require('../styles')
const SignInScreen = ({ navigation : { goBack }, navigation }) => {
    const input = useSelector((state) => state.auth)
    const dispatch = useDispatch()

    useEffect(() => {
        if(input.isValid){
            axios.post(endpoint.signin, 
                {
                    email : input.email,
                    password : input.password,
                    returnSecureToken : true
                },{
                    headers : { "Content-Type": "application"},
                })
            .then((response) => {
                const data = response.data
                console.log("response ", data)
                dispatch(setInputNotValid({signed : true, id : data.localId}))
            })
            .catch((error) => {
                console.log("Request URL", error.response.config.url)
                console.log("Request Body", error.response.config.data)
                console.log("Error Data", error.response.data)
                dispatch(setInputNotValid())  
            })
        }
    }, [input.isValid])

    return (
        <BaseBgV2
            title="Sign In"
            back={() => goBack()}
            layout={
                <>
                    <View style={{marginBottom : 12}}>
                        <TextInputComponent 
                            title="Email"
                            value={input.email}
                            changeText={(value) => dispatch(setEmail(value))}/>
                    </View>
                    <TextInputComponent 
                        title="Password"
                        value={input.password}
                        changeText={(value) => dispatch(setPassword(value))}/>
                    <View stye={{marginVertical : 8}}>
                        <Text 
                            style={styles.button.textAsButton}
                            onPress={() => ToastAndroid.show("Coming soon", ToastAndroid.SHORT)}>Forget Password</Text>
                    </View>
                    <ButtonComponent
                        title="Submit"
                        onPress={() => dispatch(submitSignIn())}/>

                    <View style={{flexDirection : "row", justifyContent : "center"}}>
                        <Text style={styles.textInput.label}>Not a member?</Text>
                        <Text 
                            style={styles.button.textAsButton}
                            onPress={() => navigation.navigate("SignUpScreen")}>Register here</Text>
                    </View>
                </>  
            }/>  
    )
}

export default SignInScreen