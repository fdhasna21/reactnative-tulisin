import { View, Text } from 'react-native'
import React, { useEffect } from 'react'
import { Icon } from 'react-native-elements'
import { useSelector, useDispatch } from 'react-redux'
import BaseBgV3 from '../components/base/BaseBgV3'
import ButtonComponent from '../components/ButtonComponent'
import PostListComponent from '../components/PostListComponent'
import { submitSignOut } from '../redux/slicers/AuthSlicer'
import { getMyPosts } from '../redux/slicers/PostSlicer'

const {styles, color, img} = require('../styles')
const ProfileScreen = ({ navigation }) => {
    const post = useSelector((state) => state.post)
    const auth = useSelector((state) => state.auth)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getMyPosts(auth))
    }, [post.isPostFetched])

    return (
      <BaseBgV3
          layout={
              <>
                    <Icon name="person" type="ionicons" size={100} color={color.primary}/>
                    <Text style={{...styles.text.subtitle, textAlign : "center"}}>{auth.email}</Text>
                    <Text style={{...styles.text.caption, textAlign : "center"}}>Member</Text>  
                    <ButtonComponent
                        title="Sign Out"
                        onPress={() => dispatch(submitSignOut())}/>

                    <Text style={{...styles.text.subtitle, color:"black", marginTop : 16}}>MY POSTS</Text>
                    <View style={{paddingHorizontal : -24}}>
                        <PostListComponent
                            nestedScrollEnabled
                            data={post.myPosts}
                            navigation={navigation}/> 
                    </View>
                    
              </>
          } />
    )
}

export default ProfileScreen