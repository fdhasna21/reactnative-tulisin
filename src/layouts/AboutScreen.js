import { View, Text, Image, ToastAndroid, FlatList } from 'react-native'
import React from 'react'
import BaseBgV3 from '../components/base/BaseBgV3'
import TextWithDrawableComponent from '../components/TextWithDrawableComponent'
import ButtonCircleComponent from '../components/ButtonCircleComponent'
import { authorContact, authorSkill } from '../datas'

const {styles, color, img} = require('../styles')
const AboutScreen = ({ navigation }) => {
    console.log(authorContact)
    return(
        <BaseBgV3
              layout={
                  <>
                        <Image
                            source={img.logo}
                            style={styles.image.about.logo}/>
                        <Text style={{...styles.text.paragraph, textAlign:"center"}}>Tulis.in is a journal application that allow you to write share your thought with everyone. You can use it as your own notes or communicate with people around you.</Text>

                        <View style={styles.divider}/>

                        <View style={{flexDirection : "row", marginTop : 12, marginBottom : 8}}>
                            <Image 
                                source={img.authorProfile}/>
                            <View style={{marginLeft : 12}}>
                                <Text style={styles.text.subtitle}>Fernanda Hasna</Text>
                                <Text style={styles.text.caption}>AUTHOR • ANDROID DEVELOPER</Text>
                                <TextWithDrawableComponent 
                                    iconName="graduation-cap"
                                    iconType="entypo"
                                    text="Computer Engineering, Institut Teknologi Sepuluh Nopember"/>
                                <TextWithDrawableComponent 
                                    iconName="work"
                                    iconType="materialicons"
                                    text="PT. Multipolar Technology Tbk"/>
                            </View>
                        </View>

                        <Text style={{...styles.text.subtitle, marginTop : 8}}>MY SKILLS</Text> 
                        <Text style={{...styles.text.paragraph, paddingVertical : 0, marginBottom : 4}}>My skills as developer :</Text> 
                        <FlatList 
                            style={{marginHorizontal : 1}}
                            horizontal={true}
                            data={authorSkill}
                            keyExtractor={item => item.id}
                            renderItem={({item}) => 
                                <View style={styles.card.outside}>
                                    <View style={styles.card.inside}>
                                        <Text style={{...styles.text.subtitle, fontWeight : "normal"}}>{item.title}</Text>
                                        <Text style={{...styles.text.caption, textTransform : "none"}}>{item.tools}</Text>
                                        <Image style={styles.image.about.skill} source={item.image} resizeMode="center"/>
                                        <Text style={{...styles.text.paragraph, paddingVertical : 0}}>{item.desc}</Text>
                                    </View>
                                </View>
                                
                            }/>

                        <Text style={{...styles.text.subtitle, marginTop : 8}}>CONTACT</Text> 
                        <Text style={{...styles.text.paragraph, paddingVertical : 0, marginBottom : 4}}>You can find me on :</Text> 
                        <FlatList
                            contentContainerStyle={{ justifyContent: 'center', flexDirection: 'row', flexWrap: 'wrap' }}
                            data={authorContact}
                            keyExtractor={item => item.id}
                            renderItem={({item}) => 
                                <ButtonCircleComponent
                                    contentStyle={styles.image.about.contact}
                                    image={item.image}
                                    onPress={() => ToastAndroid.show(item.id, ToastAndroid.SHORT)}/>
                            }/>  
                  </>
              }/>  
    )
}

export default AboutScreen