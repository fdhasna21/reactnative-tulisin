import { View, Text } from 'react-native'
import React, { useEffect } from 'react'
import axios from 'axios'
import BaseBgV2 from '../components/base/BaseBgV2'
import ButtonComponent from '../components/ButtonComponent'
import TextInputComponent from '../components/TextInputComponent'
import { useSelector, useDispatch } from 'react-redux'
import { setEmail, setPassword, setPasswordConfirmation, submitSignUp, setInputNotValid } from '../redux/slicers/AuthSlicer'

import { endpoint } from '../datas'
const {styles, color, img} = require('../styles')
const SignUpScreen = ({ navigation : { goBack }, navigation }) => {
    const input = useSelector((state) => state.auth)
    const dispatch = useDispatch()

    useEffect(() => {
        if(input.isValid){
            axios.post(endpoint.signup, 
                {
                    email : input.email,
                    password : input.password,
                    returnSecureToken : true
                },{
                    headers : { "Content-Type": "application"},
                })
            .then((response) => {
                const data = response.data
                console.log("response ", data)
                dispatch(setInputNotValid({signed : true, id : data.localId}))
            }) 
            .catch((error) => {
                console.log("Request URL", error.response.config.url)
                console.log("Request Body", error.response.config.data)
                console.log("Error Data", error.response.data)
                dispatch(setInputNotValid()) 
            })
        }
    }, [input.isValid])

    return (
        <BaseBgV2
            title="Sign Up"
            back={() => goBack()}
            layout={
                <>
                    <TextInputComponent 
                        title="Email"
                        value={input.email}
                        changeText={(value) => dispatch(setEmail(value))}/>
                    <TextInputComponent 
                        title="Password"
                        value={input.password}
                        changeText={(value) => dispatch(setPassword(value))}/>
                    <TextInputComponent 
                        title="Password Confirmation"
                        value={input.passwordConfirmation}
                        changeText={(value) => dispatch(setPasswordConfirmation(value))}/>
                    <ButtonComponent
                        title="Registration"
                        onPress={() => dispatch(submitSignUp())}/>

                    <View style={{flexDirection : "row", justifyContent : "center"}}>
                        <Text style={styles.textInput.label}>Have an account?</Text>
                        <Text 
                            style={styles.button.textAsButton}
                            onPress={() => {navigation.navigate("SignInScreen")}}>Sign In</Text>
                    </View>
                </>
            }/>
    )
}

export default SignUpScreen