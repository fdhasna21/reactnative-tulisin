import { View, Text, TextInput, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Icon } from 'react-native-elements'
import BaseBgV3 from '../components/base/BaseBgV3'
import ButtonComponent from '../components/ButtonComponent'
import { useDispatch, useSelector } from 'react-redux'
import { setTitle, setContent, setItem, setPostSubmited, resetItem, submitPost } from '../redux/slicers/PostItemSlicer'

import { endpoint } from '../datas'
import { getPosts } from '../redux/slicers/PostSlicer'
const {styles, color, img} = require('../styles')
const PostScreen = ({ route, navigation : { goBack } }) => {
    const item = useSelector((state) => state.postItem )
    const auth = useSelector((state) => state.auth)
    const dispatch = useDispatch()

    useEffect(() => {
        if(route.params != undefined) {
            const { data } = route.params
            dispatch(setItem({data : data, auth : auth}))
        } else {
            dispatch(resetItem(auth))
        }    
    }, [route.params])

    useEffect(() => {
        if(item.isValid){
            if(item.postId == ""){
                axios.post(endpoint.posts, {
                    title : item.title,
                    content : item.content,
                    author : auth.email,
                    created : "",
                    image : "",
                    authorId : auth.id
                },{
                    headers : { "Content-Type": "application"},
                })
                .then((response) => {
                    requestSuccess(response)
                })
                .catch((error) => {
                    requestFailed(error)
                })
            } else {
                axios.patch(endpoint.eachpost+item.postId+".json", {
                    title : item.title,
                    content : item.content
                },{
                    headers : { "Content-Type": "application"},
                })
                .then((response) => {
                    requestSuccess(response)
                })
                .catch((error) => {
                    requestFailed(error)
                })
            }
        }
    }, [item.isValid])

    const submitDelete = () => {
        axios.delete(endpoint.eachpost+item.postId+".json", {
            headers : { "Content-Type": "application"},
        })
        .then((response) => {
            requestSuccess(response)
        })
        .catch((error) => {
            requestFailed(error)
        })
    }

    const requestSuccess = (response) => {
        const data = response.data
        console.log("response ", data)
        dispatch(setPostSubmited({signed : true}))
        dispatch(getPosts())
        goBack()
    }

    const requestFailed = (error) => {
        console.log("Request URL", error.response.config.url)
        console.log("Request Body", error.response.config.data)
        console.log("Error Data", error.response.data)
        dispatch(setPostSubmited())  
    }
    
    return (
        <BaseBgV3
            layout={
                <>
                    {
                        item.image != ""
                        ? <Image
                            source={{ uri : item.image}}
                            style={styles.image.post}
                            resizeMode="cover"/>
                        : <></>
                    } 
                    
                    <TextInput
                        style={{ ...styles.text.subtitle, ...styles.textInput.post.title }}
                        editable={item.isEditable}
                        value={item.title}
                        onChangeText={(value) => dispatch(setTitle(value))}
                        placeholder="Title"
                        multiline={true}
                        maxlength={10}/>
                    <TextInput
                        style={{ ...styles.text.paragraph, ...styles.textInput.post.content }}
                        editable={item.isEditable}
                        value={item.content}
                        onChangeText={(value) => dispatch(setContent(value))}
                        placeholder="Write your thought here"
                        multiline={true}/>

                    {
                        item.isEditable 
                        ?  <>
                            {
                                item.postId != "" 
                                ? 
                                    <ButtonComponent
                                        title="Delete"
                                        style={{ backgroundColor : "red", borderColor : "red" }}
                                        onPress={() => submitDelete()}/>
                                : null   
                            }
                            <ButtonComponent
                                title="Save"
                                onPress={() => dispatch(submitPost())}/>
                          </>
                        : <>
                            <Text style={{...styles.text.caption, fontWeight : "bold" }}>Author</Text>
                                <View style={{flexDirection : "row", flexWrap : "wrap" }}>
                                    <Icon name="person" type="ionicons" size={60} color="gray"/>
                                    <View style={{marginLeft : 12}}>
                                        <Text style={{...styles.text.caption, fontWeight : "bold", textTransform : "none"}}>{item.author}</Text>
                                        <Text style={{...styles.text.caption, fontWeight : "bold", textTransform : "none"}}>{item.created}</Text>
                                    </View>
                                </View>
                          </>
                    } 
                    
                </>
            }
        />
    )
}

export default PostScreen